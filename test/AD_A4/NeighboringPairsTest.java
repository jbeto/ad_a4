package AD_A4;

import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;

public class NeighboringPairsTest {
    
    public NeighboringPairsTest() {
    }
    
    /**
     * Test of numNeighPairs method, of class NeighboringPairs.
     */
    @Test
    public void testNumNeighPairs1() {
        int[] iArr = new int[0];
        int expResult = 0;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }

    @Test
    public void testNumNeighPairs2() {
        int[] iArr = new int[1];
        int expResult = 0;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNumNeighPairs3() {
        int[] iArr = new int[2];
        iArr[0] = 1;
        iArr[1] = 1;
        int expResult = 1;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNumNeighPairs4() {
        int[] iArr = new int[2];
        iArr[0] = 1;
        iArr[1] = 2;
        int expResult = 1;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNumNeighPairs5() {
        int[] iArr = new int[10];

        for (int i = 0; i< iArr.length; i++){
            iArr[i] = i;
        }
        
        int expResult = iArr.length-1;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNumNeighPairs6() {
        int[] iArr = new int[10];

        for (int i = 0; i< iArr.length; i++){
            iArr[i] = i;
        }
        
        iArr[iArr.length-1] = 3;
        
        int expResult = 11;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNumNeighPairs7() {
        int[] iArr = new int[10];
//        int[] iArr = {};
        
        for (int i = 0; i< iArr.length; i++){
            iArr[i] = i;
        }
        
        iArr[iArr.length-1] = 3;
        
        int expResult = 11;
        int result = NeighboringPairs.numNeighPairs(iArr);
        assertEquals(expResult, result);
    }

    /**
     * Test of mergeSort method, of class NeighboringPairs.
     */
    @Test
    public void testMergeSort() {
        Random r = new Random();
        int[] iArr = new int[30];
        
        for (int i = 0; i< iArr.length; i++){
            iArr[i] = r.nextInt(100);
        }
        
        NeighboringPairs.mergeSort(iArr);
      
        for (int i = 0; i< iArr.length -1; i++){
            assertTrue(iArr[i]<= iArr[i+1]);
        }
    }
}
