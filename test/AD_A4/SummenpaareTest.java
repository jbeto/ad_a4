package AD_A4;

import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;

public class SummenpaareTest {

    public SummenpaareTest() {
    }

    /**
     * Test of findSumPairs2 method, of class Summerpaare.
     */
    @Test
    public void testFindSumPairs1() {
        Random r = new Random();
        int[] iArr = new int[100];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = r.nextInt(100);
        }
        int k = r.nextInt(150);
        assertEquals(Summenpaare.findSumPairs(iArr, k), Summenpaare.findSumPairs(iArr, k));
    }

    @Test
    public void testFindSumPairs2() {
        int[] iArr = new int[10];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = i;
        }
        int k = 10;
        int expResult = 4;
        int result = Summenpaare.findSumPairs(iArr, k);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindSumPairs3() {
        int[] iArr = new int[0];
        int k = 10;
        int expResult = 0;
        int result = Summenpaare.findSumPairs(iArr, k);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindSumPairs4() {
        int[] iArr = new int[10000];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = i;
        }
        int k = 10;
        int expResult = 5;
        int result = Summenpaare.findSumPairs(iArr, k);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindSumPairs5() {
        Random r = new Random();
        int[] iArr = new int[1000];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = r.nextInt(5);
        }
        int k = 11;
        int expResult = 0;
        int result = Summenpaare.findSumPairs(iArr, k);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindSumPairs6() {
        Random r = new Random();
        int[] iArr = new int[1000];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = r.nextInt(5) + 1;
        }
        int k = 1;
        int expResult = 0;
        int result = Summenpaare.findSumPairs(iArr, k);
        assertEquals(expResult, result);
    }

}
