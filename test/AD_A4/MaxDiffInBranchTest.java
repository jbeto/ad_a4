package AD_A4;

import org.junit.Test;
import static org.junit.Assert.*;

public class MaxDiffInBranchTest {

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDiffPair() {
        Pair<Integer, Integer> p = new Pair<>(0, 5);
        int result = MaxDiffInBranch.diffPair(p);
        assertEquals(5, result);
    }

    @Test
    public void testFindMinMax() {
        Tree t = new Tree(5, new Tree(-2, new Tree(6), new Tree(3)), new Tree(4, new Tree(8), new Tree(7)));
        int result = MaxDiffInBranch.findMinMax(t);
        assertEquals(8, result);
    }

    @Test
    public void testFindMinMax2() {
        Tree t = new Tree(5, new Tree(-2, new Tree(6), new Tree(-8)), new Tree(4, new Tree(8), new Tree(7)));
        int result = MaxDiffInBranch.findMinMax(t);
        assertEquals(13, result);
    }
}
