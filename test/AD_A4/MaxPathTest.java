/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AD_A4;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Johannes Berger
 */
public class MaxPathTest {

    public MaxPathTest() {
    }

    @Test
    public void testFindMinMax() {
        System.out.println("testFindMinMax");
        Tree t = new Tree(5, new Tree(-2, new Tree(6), new Tree(3)), new Tree(4, new Tree(8), new Tree(7)));
        int result = MaxPath.findMaxPath(t);
        assertEquals(2, result);
    }

    @Test
    public void testFindMinMax2() {
        System.out.println("testFindMinMax2");
        Tree t = new Tree(1, new Tree(2), new Tree(3, new Tree(4, new Tree(5, new Tree(6, new Tree(7), null), null), null), null));
        int result = MaxPath.findMaxPath(t);
        assertEquals(4, result);
    }

}
