package AD_A4;

public class Pair<T1, T2> {

    public final T1 _1;
    public final T2 _2;

    public Pair(T1 object1, T2 object2) {
        _1 = object1;
        _2 = object2;

    }

    @Override
    public String toString() {
        return "(" + _1 + "," + _2 + ")";
    }

}
