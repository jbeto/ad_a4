package AD_A4;

public class MaxDiffInBranch {

    public static int diffPair(Pair<Integer, Integer> p) {
        return p._2 - p._1;
    }

    private static int findMinMax(Tree t, Pair<Integer, Integer> parentMinMax) {
        Pair<Integer, Integer> myMinMax = new Pair<>(Math.min(t.x, parentMinMax._1), Math.max(t.x, parentMinMax._2));
        int maxDiffInChildTrees = diffPair(myMinMax); //currentDiff

        //Wenn einer der Kindsbäume eine größere Differenz hat -> setzen
        if (t.l != null) {
            maxDiffInChildTrees = Math.max(maxDiffInChildTrees, findMinMax(t.l, myMinMax));
        }
        if (t.r != null) {
            maxDiffInChildTrees = Math.max(maxDiffInChildTrees, findMinMax(t.r, myMinMax));
        }

        System.out.println("t.x: " + t.x + "\t- MinMax: " + myMinMax._1 + " " + myMinMax._2 + "\t- Diff: " + diffPair(myMinMax) + "\t- maxDiffInChildTrees: " + maxDiffInChildTrees);
        return maxDiffInChildTrees;
    }

    public static int findMinMax(Tree t) {
        return findMinMax(t, new Pair<>(t.x, t.x));
    }
}
