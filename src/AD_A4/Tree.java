package AD_A4;

final class Tree {

    public final int x;
    public final Tree l;
    public final Tree r;

    Tree() {
        this(0);
    }

    Tree(int x) {
        this(x, null, null);
    }

    Tree(int x, Tree l, Tree r) {
        this.x = x;
        this.l = l;
        this.r = r;
    }
}
