/* Algorithmen und Datenstrukturen
 * @title   Prakt. 4 - Aufgabe 4
 * @date    14.12.2014
 *
 * Aufgabe:
 *   geg.: binearer Baum T = (V,E) (Knoten,Kanten)
 *         r,l = liefern rechten/linken Kindknoten
 *
 *   ges.: maximale Distanz zwischen Knoten v1, v2, gegeben, dass
 *          - sie auf einem Pfad von Wurzel r bis Blatt li liegen
 *          - kein Richtungswechsel stattfindet, also
 *          (alle)x in (v1, ... , v2) :: l(x) in (v1, ... , v2) or x == v2
 *          (alle)x in (v1, ... , v2) :: r(x) in (v1, ... , v2) or x == v2
 *
 *  (x in y bedeutet, dass das Element x in der Sequenz y von Elementen gleichen Typs enthalten ist)
 *
 *
 * Vorgaben:
 *   Zeitkompl.:  O(V)
 *   Platzkompl.: O(V)
 */
package AD_A4;

public class MaxPath {

    private static int findMaxPath(Tree t, Pair<Integer, Integer> parentDirectionCounter) {
        int maxLength = Math.max(parentDirectionCounter._1, parentDirectionCounter._2);

        System.out.println("t.x: " + t.x + "\t- DirectionCounter: " + parentDirectionCounter._1 + " " + parentDirectionCounter._2 + "\t- maxLength: " + maxLength);

        if (t.l != null) {
            maxLength = Math.max(maxLength, findMaxPath(t.l, new Pair<>(parentDirectionCounter._1 + 1, 0)));
        }
        if (t.r != null) {
            maxLength = Math.max(maxLength, findMaxPath(t.r, new Pair<>(0, parentDirectionCounter._2 + 1)));
        }

        return maxLength;
    }

    public static int findMaxPath(Tree t) {
        return findMaxPath(t, new Pair<>(0, 0));
    }
}
