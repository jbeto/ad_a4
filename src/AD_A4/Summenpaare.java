package AD_A4;

import java.util.Arrays;
import java.util.Random;

public class Summenpaare {

    // O(n*n)
    static int findSumPairs(int[] iArr, int k) {
        int counter = 0;
        for (int i = 0; i < iArr.length; i++) {
            if (iArr[i] <= k) {
                for (int j = i + 1; j < iArr.length; j++) {
                    if (iArr[i] + iArr[j] == k) {
                        counter++;
                    }
                }
            }
        }
        return counter;
    }

    // O(n log(n))
    static int findSumPairs2(int[] iArr, int k) {
        int counter = 0;
        int idx, l, r;

        Arrays.sort(iArr);
        for (int i = 0; i < iArr.length -1; i++) {
            if (iArr[i] < k) {
                idx = Arrays.binarySearch(iArr, i + 1, iArr.length, k - iArr[i]);
                if (idx >= 0) {
                    counter++;
                    l = idx - 1;
                    r = idx + 1;
                    while (l > i && iArr[idx] == iArr[l]) {
                        counter++;
                        l--;
                    }
                    while (r < iArr.length && iArr[idx] == iArr[r]) {
                        counter++;
                        r++;
                    }
                }
            }
        }
        return counter;
    }

    public static void main(String[] args) {
        Random r = new Random();

        int k = 7;
        int[] iArr = new int[50000];

        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = r.nextInt(5);
//            iArr[i] = i;
        }

//        for (int j : iArr) {
//            System.out.print(j + " ");
//        }
//        System.out.println("");
        System.out.println("Es gibt " + findSumPairs2(iArr, k) + " Paare in dem Array die addiert " + k + " ergeben");
        System.out.println("Es gibt " + findSumPairs(iArr, k) + " Paare in dem Array die addiert " + k + " ergeben");
    }
}
