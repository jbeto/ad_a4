/* Algorithmen und Datenstrukturen 
 * @title   Prakt. 4 - Aufgabe 1
 * @date    12.12.2014
 * 
 * Aufgabe: 
 *   geg.: Array mit Elementen aus Z
 *   ges.: Anzahl benachbarter Werte (P,Q)
 * 
 *   !E i elem Z | 0 < =  i < N and P < A[i] < Q
 * 
 * Vorgaben:
 *   Zeitkompl.:  O( n*log(n) )
 *   Platzkompl.: O(N)
 */
package AD_A4;

import java.util.Arrays;

public class NeighboringPairs {

    static int numNeighPairs(int[] iArr) {
        int pairCount = 0;


        mergeSort(iArr);
        
        int[] countArr = new int[iArr.length];
        int c = 0;
        
        for (int i = 0; i < iArr.length; i++) {
            
            if(i==0){
                countArr[c] = 1;
            }
            else if (iArr[i - 1] == iArr[i]) {
                countArr[c]++;
            } else {
                c++;
                countArr[c]=1;
            }
        }
        

//        for (int j : iArr) {
//            System.out.print(j + " ");
//        }
//        System.out.println("");
//        
//        for (int j : countArr) {
//            System.out.print(j + " ");
//        }
//        System.out.println("");
        
        
        for (int i = 0; i < countArr.length -1; i++) {
            if (countArr[i] > 1){
                int fac = 1;
                
                for (int j = 1; j <= countArr[i] - 1; j++) {
                    fac *=j;
                }
                
                pairCount += fac;
            }
            
            pairCount += countArr[i] * countArr[i + 1];
        }
        

        return pairCount;
    }

    /* MergeSort: Zeitkompl.:  O( n*log(n) ), Platzkompl.: O( n ) */
    static void mergeSort(int[] iArr) {
        if (iArr.length > 1) {
            int m = (iArr.length + 1) / 2;
            int lArr[] = Arrays.copyOfRange(iArr, 0, m);
            int rArr[] = Arrays.copyOfRange(iArr, m, iArr.length);

            mergeSort(lArr);
            mergeSort(rArr);
            merge(iArr, lArr, rArr);
        }
    }

    static void merge(int[] iArr, int[] left, int[] right) {
        int l = 0;
        int r = 0;

        while (l < left.length || r < right.length) {
            if (l == left.length) {
                iArr[l + r] = right[r];
                r++;
            } else if (r == right.length) {
                iArr[l + r] = left[l];
                l++;
            } else if (left[l] <= right[r]) {
                iArr[l + r] = left[l];
                l++;
            } else {
                iArr[l + r] = right[r];
                r++;
            }
        }
    }

    static void test(int[] iArr) {
        int num, sortedNum;
        System.out.println("iArr (unsortiert): " + Arrays.toString(iArr));
        num = numNeighPairs(iArr);
        System.out.println("Anzahl ben. Paare: " + num);

        mergeSort(iArr);
        System.out.println("iArr   (sortiert): " + Arrays.toString(iArr));
        sortedNum = numNeighPairs(iArr);
        System.out.println("Anzahl ben. Paare: " + sortedNum);
    }

    public static void main(String[] args) {
        System.out.println("AD Praktikum - Praktikum 4 - Aufgabe 1:\n");
        int iArr[] = {2, 1, 5, 4, 7, 5};
        test(iArr);

    }
    
}
